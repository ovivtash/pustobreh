﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Speech.Synthesis;
using System.Text;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Pustobreh
{
    public partial class frmMain : Form
    {
        private bool gonnaClose = false;
        private SpeechSynthesizer synth;
        private string[] subTopics = {""};
        byte[] qosLevels = { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE };

        public MqttClient mqtt;

        private bool MqttConnected
        {
            get { return (mqtt != null) && mqtt.IsConnected; }
        }

        void UpdateAccessibility()
        {
            try
            {
                if (!InvokeRequired)
                {
                    propertyGrid1.Enabled = !MqttConnected;
                    btnConnect.Enabled = !MqttConnected;
                    btnDisconnect.Enabled = MqttConnected;
                    btnSetActiveVoice.Enabled = !MqttConnected;
                }
                else
                {
                    Invoke(new Action(UpdateAccessibility));
                }
            }
            catch(ObjectDisposedException){}
            catch(InvalidAsynchronousStateException) { }
        }

        private void SynthInit(bool setVoice = true)
        {
            if (setVoice && !String.IsNullOrEmpty(Properties.Settings.Default.VoiceName))
            {
                synth.SelectVoice(Properties.Settings.Default.VoiceName);
            }
            synth.SetOutputToDefaultAudioDevice();
            if (Properties.Settings.Default.VoiceRate < -10 || Properties.Settings.Default.VoiceRate > 10)
            {
                Properties.Settings.Default.VoiceRate = 0;
            }
            synth.Volume = 100;
            synth.Rate = Properties.Settings.Default.VoiceRate;
        }

        void Connect()
        {
            Log("Connecting...");

            subTopics[0] = String.Format("pustobreh/{0}", Properties.Settings.Default.InstanceName);

            //mqtt = new MqttClient(IPAddress.Parse("192.168.3.10"));
            mqtt = new MqttClient(Properties.Settings.Default.MqttHost);
            //mqtt.ProtocolVersion=MqttProtocolVersion.Version_3_1;
            
            mqtt.MqttMsgPublishReceived += MqttOnMsgReceived;
            mqtt.ConnectionClosed += (sender, args) =>
            {
                UpdateAccessibility();
                Log("Connection closed");
            };
            mqtt.MqttMsgSubscribed += (sender, args) => Log("Subscribed");
            mqtt.MqttMsgUnsubscribed += (sender, args) => Log("Unsubscribed");

            var connResult = mqtt.Connect(String.Format("{0}-{1}", Application.ProductName, Guid.NewGuid()));
            switch (connResult)
            {
                case 0:
                    Log("Connected!");
                    SynthInit();
                    mqtt.Subscribe(subTopics, qosLevels);
                    break;
                case 1:
                    Log("Connection refused, unacceptable protocol version");
                    break;
                case 2:
                    Log("Connection refused, identifier rejected");
                    break;
                case 3:
                    Log("Connection refused, server unavailable");
                    break;
                case 4:
                    Log("Connection refused, bad user name or password");
                    break;
                case 5  :
                    Log("Connection refused, not authorized");
                    break;
            }
            UpdateAccessibility();
        }

        void Disconnect()
        {
            if (MqttConnected)
            {
                Log("Disconnecting...");
                mqtt.Unsubscribe(subTopics);
                try
                {
                    mqtt.Disconnect();
                }
                catch(Exception){}
            }
        }

        private void MqttOnMsgReceived(object sender, MqttMsgPublishEventArgs mqttMsg)
        {
            string text = Encoding.Default.GetString(mqttMsg.Message);
            Log("Got smth to say: " + text);
            synth.Speak(text);
        }

        public frmMain()
        {
            InitializeComponent();
            Icon = Properties.Resources.appIcon;
            synth = new SpeechSynthesizer();
            synth.Volume = 100;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = String.Format("{0} v{1}", Application.ProductName, Application.ProductVersion);
            Visible = false;
            notifyIcon1.Text = Application.ProductName;
            Hide();

            propertyGrid1.SelectedObject = Properties.Settings.Default;
            
            InstalledVoice[] voices =synth.GetInstalledVoices().ToArray();
            cmbVoiceNames.Items.Clear();
            foreach (var voice in voices)
            {
                cmbVoiceNames.Items.Add(voice.VoiceInfo.Name);
            }
            try
            {
                Connect();
            }
            catch (Exception ex)
            {
                Log(ex.Message);                
            }

            //synth.SelectVoiceByHints(VoiceGender.Male, VoiceAge.NotSet, 0, CultureInfo.CurrentCulture);
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MqttConnected)
            {
                mqtt.Disconnect();
            }
        }

        private void btnTestSynth_Click(object sender, EventArgs e)
        {
            
            synth.SelectVoice(cmbVoiceNames.SelectedItem.ToString());
            SynthInit(false);
            synth.Speak(txtTestPhrase.Text);
        }

        public void Log(string what)
        {
            try
            {
                if (!InvokeRequired)
                {
                    txtLog.AppendText(String.Format("[{0}] {1}\n", DateTime.Now, what));
                }
                else
                {
                    Invoke(new Action<string>(Log), what);
                }
            }
            catch(ObjectDisposedException){}
            catch(InvalidAsynchronousStateException) { }
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            gonnaClose = true;
            Application.Exit();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void btnSetActiveVoice_Click(object sender, EventArgs e)
        {
            if (cmbVoiceNames.SelectedItem != null)
            {
                Properties.Settings.Default.VoiceName = cmbVoiceNames.SelectedItem.ToString();
                propertyGrid1.Update();
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                this.Visible = !this.Visible;
                this.WindowState = this.Visible ? FormWindowState.Normal : FormWindowState.Minimized;
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!gonnaClose)
            {
                e.Cancel = true;
                Visible = false;
            }
        }
    }
}
