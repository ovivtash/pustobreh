﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pustobreh
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            initSettings();
            Application.Run(new frmMain());
        }

        static void initSettings()
        {
            if (String.IsNullOrEmpty(Properties.Settings.Default.InstanceName))
            {
                Properties.Settings.Default.InstanceName = Environment.MachineName;
            }
        }

    }
}
