### Pustobreh

A simple WinForms app that uses TTS engine to say whatever comes from an MQTT topic.

After startup, it hides in the system tray. To open main form, double-click the tray icon with middle mouse button.