﻿namespace Pustobreh
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmbVoiceNames = new System.Windows.Forms.ToolStripComboBox();
            this.txtTestPhrase = new System.Windows.Forms.ToolStripTextBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnConnect = new System.Windows.Forms.ToolStripButton();
            this.btnDisconnect = new System.Windows.Forms.ToolStripButton();
            this.btnTestSynth = new System.Windows.Forms.ToolStripButton();
            this.btnSetActiveVoice = new System.Windows.Forms.ToolStripButton();
            this.btnSaveSettings = new System.Windows.Forms.ToolStripButton();
            this.btnQuit = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Right;
            this.propertyGrid1.Location = new System.Drawing.Point(580, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyGrid1.Size = new System.Drawing.Size(298, 398);
            this.propertyGrid1.TabIndex = 4;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnConnect,
            this.btnDisconnect,
            this.toolStripSeparator1,
            this.cmbVoiceNames,
            this.txtTestPhrase,
            this.btnTestSynth,
            this.btnSetActiveVoice,
            this.toolStripSeparator2,
            this.btnSaveSettings,
            this.btnQuit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(580, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // cmbVoiceNames
            // 
            this.cmbVoiceNames.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.cmbVoiceNames.Name = "cmbVoiceNames";
            this.cmbVoiceNames.Size = new System.Drawing.Size(150, 25);
            // 
            // txtTestPhrase
            // 
            this.txtTestPhrase.Name = "txtTestPhrase";
            this.txtTestPhrase.Size = new System.Drawing.Size(100, 25);
            this.txtTestPhrase.Text = "this is me now";
            this.txtTestPhrase.ToolTipText = "TTS Test phrase";
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(0, 25);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(580, 373);
            this.txtLog.TabIndex = 6;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnConnect
            // 
            this.btnConnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConnect.Image = global::Pustobreh.Properties.Resources.Connect_16x;
            this.btnConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(23, 22);
            this.btnConnect.Text = "Connect";
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDisconnect.Image = global::Pustobreh.Properties.Resources.Disconnect_16x;
            this.btnDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(23, 22);
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnTestSynth
            // 
            this.btnTestSynth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTestSynth.Image = global::Pustobreh.Properties.Resources.PlaybackPreview_16x;
            this.btnTestSynth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTestSynth.Name = "btnTestSynth";
            this.btnTestSynth.Size = new System.Drawing.Size(23, 22);
            this.btnTestSynth.Text = "Test TTS";
            this.btnTestSynth.Click += new System.EventHandler(this.btnTestSynth_Click);
            // 
            // btnSetActiveVoice
            // 
            this.btnSetActiveVoice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSetActiveVoice.Image = global::Pustobreh.Properties.Resources.Checkmark_16x;
            this.btnSetActiveVoice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetActiveVoice.Name = "btnSetActiveVoice";
            this.btnSetActiveVoice.Size = new System.Drawing.Size(23, 22);
            this.btnSetActiveVoice.Text = "Set voice as active";
            this.btnSetActiveVoice.Click += new System.EventHandler(this.btnSetActiveVoice_Click);
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveSettings.Image = global::Pustobreh.Properties.Resources.Save_16x;
            this.btnSaveSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(23, 22);
            this.btnSaveSettings.Text = "Save settings";
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnQuit.Image = global::Pustobreh.Properties.Resources.Close_16x;
            this.btnQuit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(23, 22);
            this.btnQuit.Text = "Quit";
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 398);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.propertyGrid1);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnConnect;
        private System.Windows.Forms.ToolStripButton btnDisconnect;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox txtTestPhrase;
        private System.Windows.Forms.ToolStripButton btnTestSynth;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.ToolStripComboBox cmbVoiceNames;
        private System.Windows.Forms.ToolStripButton btnSetActiveVoice;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnSaveSettings;
        private System.Windows.Forms.ToolStripButton btnQuit;
    }
}

